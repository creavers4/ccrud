<?php include_once('header.php'); ?>
	<div class="container">

		<!-- Codeigniter's form opening -->
		<?php echo form_open('Welcome/save', ['class'=>'form-horizontal']);?>
			<fieldset>
				<legend>Add New Post</legend>
				<div class="form-group">
					<label for="inputEmail" class="col-md-2 control-label">Title</label>
					<div class="col-md-5">
						<!-- Codeigniter's input field -->
						<?php echo form_input(['name'=>'title', 'placeholder'=>'Title', 'class'=>'form-control']);?>
					</div>

					<div class="col-md-5">
						<?php echo form_error('title', '<div class = "text-danger">', '</div>'); ?>
					</div>
					</div>

					<div class="form-group">
						<label for="textArea" class="col-md-2 control-label">Description</label>
						<div class="col-md-5">

						<!-- Codeigniter's textarea field -->
							<?php echo form_textarea(['name'=>'description', 'placeholder'=>'Description', 'class'=>'form-control']);?>
						</div>

						<div class="col-md-5">
							<?php echo form_error('description', '<div class = "text-danger">', '</div>'); ?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-10 col-md-offset-2">
						<!-- Codeigniter's Submit Button-->
						<?php echo form_submit(['name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-default']);?>
						<!-- Codeigniter's Button-->
						<?php echo anchor('welcome', 'Back', ['class'=>'btn btn-default']);?>
					</div>
				</div>
			</fieldset>

			<!-- Codeigniter's form closing -->
		<?php echo form_close();?>
	</div>
<?php include_once('footer.php'); ?>