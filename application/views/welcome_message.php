<?php  include_once('header.php');?>
	<div class="container">
	<h3>View All Posts</h3>
	<?php echo anchor('welcome/create', 'Add Post', ['class'=>'btn btn-primary']);?>
	<?php if($msg = $this->session->flashdata('msg')):?>
			<?php echo $msg; ?>
	<?php endif;?>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Title</th>
					<th>Description</th>
					<th>Creation Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<!-- Accessing the data received from the controller(Welcome) -->
			<?php if(count($posts)):?>
				<?php foreach($posts as $post):?>
				<tr>
				<!-- The style inside the <td> makes the content wrapped -->
					<td style="word-wrap: break-word;min-width: 160px;max-width: 160px;"><?php echo $post->title;?></td>
					<td style="word-wrap: break-word;min-width: 160px;max-width: 160px;"><?php echo $post->description;?></td>
					<td style="word-wrap: break-word;min-width: 160px;max-width: 160px;"><?php echo $post->date_created;?></td>
					<td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">
						<?php echo anchor("welcome/view/{$post->id}", 'View', ['class'=>'label label-primary']);?>
						<?php echo anchor("welcome/update/{$post->id}", 'Update', ['class'=>'label label-success']);?>
						<?php echo anchor("welcome/delete/{$post->id}", 'Delete', ['class'=>'label label-danger']);?>
					</td>
				</tr>
			<?php endforeach;?>
			<?php else:?>
				<td></td>
				<td></td>
				<td>No Records Found!</td>
				<td></td>
			<?php endif;?>
			</tbody>
		</table>
	</div>
	<?php  include_once('footer.php');?>