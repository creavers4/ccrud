<?php include_once('header.php'); ?>
<div class="container">
	<h3><?php echo $post->title; ?></h3>
	<p>(<?php echo $post->date_created; ?>)</p>
	<p style="word-wrap: break-word;min-width: 500px;max-width: 1000px;"><?php echo $post->description; ?></p>
	<?php echo anchor('welcome', 'Back', ['class'=>'btn btn-default']);?>
</div>
<br>
<?php include_once('footer.php'); ?> 