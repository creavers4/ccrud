<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('queries');
	}

	public function index()
	{
		//calling a function (getPosts()) which is located inside queries model
		$posts = $this->queries->getPosts();
		// Sending data from Controller(Welcome) into View(welcome_message)
		$this->load->view('welcome_message', ['posts' => $posts]);
	}

	public function create(){
		$this->load->view('create');
	}

	public function save(){
		//Setting validation rules for the form elements
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		if ($this->form_validation->run()){
			$data = $this->input->post();
			$current_date = date('Y-m-d');
			$data['date_created'] = $current_date;
			unset($data['submit']);

			if($this->queries->addPost($data)){
				$this->session->set_flashdata('msg', 'Saved Successfully');
			}else{
				$this->session->set_flashdata('msg', 'Failed To Save');
			}
			return redirect('welcome');
		}else{
			$this->load->view('create');
		}
	}

	public function update($id){
		$post = $this->queries->getSinglePost($id);
		$this->load->view('update', ['post'=>$post]);
	}

	public function change($id){
		//Setting validation rules for the form elements
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		if ($this->form_validation->run()){
			$data = $this->input->post();
			$current_date = date('Y-m-d');
			$data['date_created'] = $current_date;
			unset($data['submit']);
			
			/*
			echo "<pre>";
			print_r($data);
			echo "<pre>";
			exit();
			*/

			if($this->queries->updatePost($data, $id)){
				$this->session->set_flashdata('msg', 'Updated Successfully');
			}else{
				$this->session->set_flashdata('msg', 'Failed To Update');
			}
			return redirect('welcome');
		}else{
			$this->load->view('create');
		}
	}

	public function view($id){
		$post = $this->queries->getSinglePost($id);
		$this->load->view('view', ['post'=>$post]);
	}

	public function delete($id){
		if($this->queries->deletePost($id)){
				$this->session->set_flashdata('msg', 'Deleted Successfully');
			}else{
				$this->session->set_flashdata('msg', 'Failed To Delete');
			}
			return redirect('welcome');
	}
}